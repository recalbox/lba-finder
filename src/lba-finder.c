#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>

#define MBR_SIGNATURE 0xAA55

struct chs {
  uint8_t head;
  uint8_t sector;
  uint8_t cylinder;
}__attribute__((packed));

struct mbr {
  uint8_t code[440];
  int32_t disk_signature;
  int16_t unused;
  struct mbr_partition {
    uint8_t status;
    struct chs first_sector;
    uint8_t partition_type;
    struct chs last_sector;
    uint32_t first_sector_lba;
    uint32_t sectors;
  }__attribute__((packed)) partition[4];
  uint16_t mbr_signature;
}__attribute__((packed));

typedef struct mbr MBR;

void print_usage();
MBR * read_mbr(char*);
void free_mbr(MBR*);

void print_usage() {
  printf("usage:");
  printf("  lba <disk/image>\n");
}

/*
 * read one sector from disk
 */
struct mbr *read_mbr(char * disk) {
  MBR * mbr;
  FILE * fh_disk;

  fh_disk = fopen(disk, "rb");
  if (!fh_disk) {
    perror("");
    return NULL;
  }

  mbr = malloc(512);
  /* fail if I can't read 512b
   */
  if (fread(mbr, 512, 1, fh_disk) != 1) {
    free(mbr);
    mbr = NULL;
  }
  fclose(fh_disk);
  return mbr;
}

/*
 * free mbr
 */
void free_mbr(MBR * mbr) {
  free(mbr);
}

int main(int argc, char ** argv) {
  struct mbr * diskmbr;
  uint32_t C=1024,H=255,S=63;
  uint32_t c,h,s;
  if (!argv[1]) {
    print_usage();
    exit(1);
  }

  diskmbr = read_mbr(argv[1]);

  if (!diskmbr) {
    fprintf(stderr, "Error read MBR\n");
    exit(1);
  }

  c = diskmbr->partition[0].first_sector.cylinder;
  h = diskmbr->partition[0].first_sector.head;
  s = diskmbr->partition[0].first_sector.sector;
  printf("%d\n", (c*H+h)*S+(s-1));
  free_mbr(diskmbr);
}
